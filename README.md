# APP - INDICADORES

Este proyecto contiene código fuente de una aplicación Node.js utilizando Express.


## Requisitos e Instalación 🔧


- Node.js - [Install Node.js 10](https://nodejs.org/en/), including the NPM package management tool.
- Angular - [Angular cli](https://cli.angular.io/)
- Git -[Git](https://www.atlassian.com/git/tutorials/install-git)

Ahora que tienes lo necesario , necesitas hacer el clon del proyecto :

```
https://gitlab.com/diegoescaff/front-indicadores.git
```

lo primero es ingresar a la carpeta `/front-indicadores` y ejecutar el siguiente comando que instala las dependencias definidas en el archivo `package.json`

```
npm install
```

La aplicación viene previamente configurada con credenciales para realizar pruebas de inmediato, pero puedes cambiarlas por tus propias credenciales en el archivo `.config/config.ts`.

> **OJO 👀:** Si utilizas un puerto de tu preferencia recuerda cambiarlo, solo en ese campo ademas este debe tener realicion con el la url definida en tu back.


```bash
    export const URL_SERVICIOS = "http://localhost:3000/api"
```


## Despliegue 📦

```bash
ng serve -o
```

Ahora dirígete en tu navegador a `http://localhost:4200/`

