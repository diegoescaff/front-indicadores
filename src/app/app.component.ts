import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "INDICADORES ECONÓMICOS";
  subtitle = "Debes mover el mouse sobre las tarjetas para ver su detalle !";
}
