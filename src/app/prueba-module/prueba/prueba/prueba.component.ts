import { Component, OnInit } from "@angular/core";
import { HitsService } from "./prueba.service";
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from "@angular/animations";

@Component({
  selector: "app-prueba",
  templateUrl: "./prueba.component.html",
  styleUrls: ["./prueba.component.css"],
  animations: [
    trigger("flipState", [
      state(
        "active",
        style({
          transform: "rotateY(179deg)",
        })
      ),
      state(
        "inactive",
        style({
          transform: "rotateY(0)",
        })
      ),
      transition("active => inactive", animate("500ms ease-out")),
      transition("inactive => active", animate("500ms ease-in")),
    ]),
  ],
})
export class PruebaComponent implements OnInit {
  constructor(public hitsService: HitsService) {}

  indicadores: { key: string; name: string; unit: string; value: number , date:Date }[] = [];
  estado : number;
  ngOnInit() {
    this.find();
  }

  flip: string = "inactive";

  toggleFlip() {
    this.flip = this.flip == "inactive" ? "active" : "inactive";
  }

  find() {
    this.hitsService.findAll().subscribe((res: any) => {
      this.estado = res.estatus;
      this.indicadores = res.data;
    });
  }
}
