import { Injectable } from "@angular/core";
import { HttpResponse, HttpClient } from "@angular/common/http";

import { URL_SERVICIOS } from "../../../../config/config";

@Injectable({
  providedIn: "root"
})
export class HitsService {
  constructor(public http: HttpClient) {}

  findAll() {
    const url = URL_SERVICIOS + "/indicadores";
    return this.http.get(url);
  }


}
